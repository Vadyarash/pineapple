package com.example.vadim.pineapplerex;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

class GameView extends SurfaceView implements Runnable{

    public static int maxX = 20; // размер по горизонтали
    public static int maxY = 28; // размер по вертикали
    public static float unitW = 0; // пикселей в юните по горизонтали
    public static float unitH = 0; // пикселей в юните по вертикали
    private boolean firstTime = true;
    private boolean gameRunning = true;
    private Pineapple pineapple;
    private Thread gameThread = null;
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;
    private ArrayList<Knife> knifeArrayList = new ArrayList<>(); // тут будут харанится ножи
    private final int KNIFE_INTERVAL = 50; // время через которое появляются ножи
    private int currentTime = 0;

    public GameView(Context context) {
        super(context);
        surfaceHolder = getHolder();
        paint = new Paint();
        gameThread = new Thread(this);
        gameThread.start();
    }
    @Override
    public void run() {
        while (gameRunning) {
            update();
            draw();
            checkCollision();
            checkIfNewKnife();
            control();
        }
    }

    private void update() {
        if(!firstTime) {
            pineapple.update();
            for (Knife knife : knifeArrayList) knife.update();
        }
    }

    private void draw() {
        if (surfaceHolder.getSurface().isValid()) {

            if(firstTime){ // инициализация при первом запуске
                firstTime = false;
                unitW = surfaceHolder.getSurfaceFrame().width()/maxX;
                unitH = surfaceHolder.getSurfaceFrame().height()/maxY;

                pineapple = new Pineapple(getContext()); // добавляем ананасик
            }

            canvas = surfaceHolder.lockCanvas();
            canvas.drawColor(Color.CYAN);

            pineapple.drow(paint, canvas); // рисуем ананасик

            for(Knife knife: knifeArrayList){ // рисуем ножи
                knife.drow(paint, canvas);
            }

            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void control() { // пауза на 17 миллисекунд
        try {
            gameThread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void checkCollision(){ // перебираем все ножи и проверяем не касается ли один из них ананасика
        for (Knife knife : knifeArrayList) {
            if(knife.isCollision(pineapple.x, pineapple.y, pineapple.size)){
                // игрок проиграл
                gameRunning = false; // останавливаем игру

            }
        }
    }

    private void checkIfNewKnife(){ // каждые 50 итераций добавляем новый нож
        if(currentTime >= KNIFE_INTERVAL){
            Knife knife = new Knife(getContext());
            knifeArrayList.add(knife);
            currentTime = 0;
        }else{
            currentTime ++;
        }
    }

}
