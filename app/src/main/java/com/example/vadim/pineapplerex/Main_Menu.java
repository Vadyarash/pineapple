package com.example.vadim.pineapplerex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main_Menu extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        final Button startButton = (Button) findViewById(R.id.startButton); // определение кнпокпи
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // определение метода клика по кнопке
                startButton.setBackgroundResource(R.drawable.pressbutton); // смена картинки на другую, что создаёт эффект нажатия
                try { // блок который делает переход с главного меню в игровую область сразу после нажатия кнопки
                    Intent intent = new Intent(Main_Menu.this, MainActivity.class); startActivity(intent); finish();
                } catch (Exception e) {

                }

            }
        });
    }
}

